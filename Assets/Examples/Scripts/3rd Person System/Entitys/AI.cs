﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI : Entity
{
    public LayerMask mask;
    Action<SkillController> onGroundClicked;
    float timer;
    float cooldown = 15;
    SkillingObject[] skillObjects;
    int randomRange;
    public override void ExtendStart()
    {
        skillObjects = FindObjectsOfType<SkillingObject>();
        cooldown = UnityEngine.Random.Range(1, 5);
        //skillObjects.add
    }
    public override void DetectInput()
    {
        if (Time.time - timer > cooldown)
        {
            timer = Time.time;
            cooldown = UnityEngine.Random.Range(5, 35);
            // Walk(new Vector3(UnityEngine.Random.Range(-30, 30), 0, UnityEngine.Random.Range(-30, 30)));
            skillObjects[randomRange].CancelAction(GetComponent<SkillController>(), m_Skill);
            randomRange = UnityEngine.Random.Range(0, skillObjects.Length - 1);
            Walk(skillObjects[randomRange].transform.position, skillObjects[randomRange].PerformAction);
        }
        //onGroundClicked = tempObject.GetComponent<BaseObject>().CancelAction;
        
    }

}
