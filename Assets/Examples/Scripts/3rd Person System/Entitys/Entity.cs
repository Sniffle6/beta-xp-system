﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent), typeof(Animator))]
public abstract class Entity : MonoBehaviour
{
    const string RUN_ANIMATION = "Run";


    protected IEnumerator m_Skill;
    NavMeshAgent m_Agent;
    Animator m_Anim;
    public bool isWalking;
    public bool isSkilling;
    private Action<SkillController, IEnumerator> _onComplete;
    // Use this for initialization
    void Start()
    {
        m_Agent = GetComponent<NavMeshAgent>();
        m_Anim = GetComponent<Animator>();
        ExtendStart();

    }
    public abstract void ExtendStart();
    private void Update()
    {
        DetectInput();
        StopWalk();
    }
    public void Walk(Vector3 position)
    {
        m_Anim.SetBool(RUN_ANIMATION, true);
        isWalking = true;
        m_Agent.SetDestination(position);

    }
    public void Walk(Vector3 position, Action<SkillController, IEnumerator> onComplete)
    {
        m_Anim.SetBool(RUN_ANIMATION, true);
        isWalking = true;
        m_Agent.SetDestination(position);
        _onComplete = onComplete;
    }
    public void StopWalk()
    {
        if (!m_Agent.pathPending && isWalking && m_Anim.GetBool(RUN_ANIMATION) == true)
        {
            if (m_Agent.remainingDistance < 0.1f)
            {
                m_Anim.SetBool(RUN_ANIMATION, false);
                isWalking = false;
                if (_onComplete != null)
                    _onComplete.Invoke(GetComponent<SkillController>(), m_Skill);
                _onComplete = null;
            }
        }
    }
    public abstract void DetectInput();

}
