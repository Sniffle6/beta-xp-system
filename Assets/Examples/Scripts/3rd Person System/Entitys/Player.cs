﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Player : Entity
{
    RaycastHit m_HitInfo = new RaycastHit();
    public LayerMask mask;

    GameObject tempObject;
    Action<SkillController, IEnumerator> onGroundClicked;
    public override void DetectInput()
    {
        if (Input.GetMouseButtonDown(0))
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out m_HitInfo, 100, mask))
            {
                tempObject = m_HitInfo.collider.gameObject;

                if (onGroundClicked != null)
                {
                    onGroundClicked.Invoke(GetComponent<SkillController>(), m_Skill);
                    onGroundClicked = null;
                }
                if (tempObject.GetComponent<BaseObject>())
                {
                    onGroundClicked = tempObject.GetComponent<BaseObject>().CancelAction;
                    Walk(m_HitInfo.point, tempObject.GetComponent<BaseObject>().PerformAction);
                }
                else
                {
                    Walk(m_HitInfo.point);
                }
            }
        }
    }

    public override void ExtendStart()
    {
    }
}
