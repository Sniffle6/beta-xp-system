﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour
{
    public Transform _target;
    public Vector3 _offset;
    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (!_target)
                _target = GameObject.FindGameObjectWithTag("Player").transform;
        if (_target)
            transform.position = _target.position + _offset;
    }
}
