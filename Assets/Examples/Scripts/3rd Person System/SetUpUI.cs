﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetUpUI : MonoBehaviour
{
    public SkillListObject _skillList;
    public GameObject _uiElement;
    public Transform _parentElement;
    // Use this for initialization
    Transform temp;
    List<GameObject> ui = new List<GameObject>();
    void Start()
    {
        if (_skillList.skills.Count <= 0)
            return;
        for (int i = 0; i < _skillList._stat.List.Count; i++)
        {
            temp = Instantiate(_uiElement, _parentElement).transform;
            temp.localPosition = new Vector3(0, -i * 25, 0);

            temp.GetChild(0).GetComponent<Text>().text = _skillList._stat.List[i].skillType.name;
            temp.GetChild(1).GetComponent<Text>().text = _skillList.GetSkill(_skillList._stat.List[i].skillType).ModifiedLevel.ToString();

            ui.Add(temp.gameObject);
        }
    }
    
    public void UpdateUI()
    {
        if (_skillList.skills.Count <= 0)
            return;
        for (int i = 0; i < ui.Count; i++)
        {
            ui[i].transform.GetChild(0).GetComponent<Text>().text = _skillList._stat.List[i].skillType.name;
            ui[i].transform.GetChild(1).GetComponent<Text>().text = _skillList.GetSkill(_skillList._stat.List[i].skillType).ModifiedLevel.ToString();
        }
    }
}
