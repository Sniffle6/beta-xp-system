﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseObject : MonoBehaviour {

    public abstract void PerformAction(SkillController entity, IEnumerator corutine);
    public abstract void CancelAction(SkillController entity, IEnumerator corutine);
}
