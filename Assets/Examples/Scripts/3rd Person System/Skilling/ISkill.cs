﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISkill  {
    void StartSkill(SkillController entity, IEnumerator corutine);
    void StopSkill(SkillController entity, IEnumerator corutine);

}
