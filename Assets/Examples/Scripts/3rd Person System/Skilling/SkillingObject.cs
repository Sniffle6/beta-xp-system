﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillingObject : BaseObject, ISkill
{
    public SkillObject _requiredSkill;
    public int _requiredLevel;
    public int _experienceGained;
    public string _animationToUse;
    readonly float cooldownTime = 1f;
    public override void PerformAction(SkillController entity, IEnumerator corutine)
    {
        StartSkill(entity, corutine);
    }
    public override void CancelAction(SkillController entity, IEnumerator corutine)
    {
        StopSkill(entity, corutine);
    }

    public void StartSkill(SkillController entity, IEnumerator corutine)
    {
        if (!entity.HasRequiredLevel(_requiredSkill, _requiredLevel))
        {
            StopSkill(entity, corutine);
            return;
        }
        entity.GetComponent<Animator>().SetBool(_animationToUse, true);
        entity.transform.LookAt(transform);
        entity.GetComponent<Entity>().isSkilling = true;
        corutine = SkillCorutine(entity);
        StartCoroutine(corutine);
    }
    public void StopSkill(SkillController entity, IEnumerator corutine)
    {
        entity.GetComponent<Animator>().SetBool(_animationToUse, false);
        entity.GetComponent<Entity>().isSkilling = false;
        if (corutine != null)
        {
            StopCoroutine(corutine);
            corutine = null;
        }
    }
    IEnumerator SkillCorutine(SkillController entity)
    {
        while (entity.GetComponent<Entity>().isSkilling)
        {
            yield return new WaitForSecondsRealtime(cooldownTime);
            entity.AddSkillXp(_requiredSkill, _experienceGained);
        }
    }
}
