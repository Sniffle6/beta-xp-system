﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddExperience : MonoBehaviour {
    public SkillListObject entity;
    public SkillObject skillToAddXp;
    public int xpToGain;
	public void AddExp()
    {
        entity.GetSkill(skillToAddXp).AddSkillXP(xpToGain);
    }
}
