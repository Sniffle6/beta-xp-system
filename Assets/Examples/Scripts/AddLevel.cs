﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddLevel : MonoBehaviour
{
    public SkillListObject entity;
    public SkillObject skill;
    public int levelToGain;
    public Text text;
    CharacterScreenPlayer player;
    Button button;
    private void Start()
    {
        player = FindObjectOfType<CharacterScreenPlayer>();
        button = GetComponent<Button>();

    }
    private void OnEnable()
    {
        entity.GetSkill(skill).Level.RegisterModEvent(UpdateText);
    }
    private void OnDisable()
    {
        if (entity.GetSkill(skill) != null)//make sure its unregistering
            entity.GetSkill(skill).Level.UnregisterModEvent(UpdateText);
    }
    private void Update()
    {
        ColorBlock cb = button.colors;
        if (player.levelUpPoints > 0)
        {
            cb.normalColor = Color.white;
            cb.highlightedColor = Color.white;
            cb.pressedColor = new Color(0.8f, 0.8f, 0.8f);
        }
        else
        {
            cb.normalColor = Color.gray;
            cb.highlightedColor = Color.gray;
            cb.pressedColor = Color.gray;
        }
        button.colors = cb;
    }
    void UpdateText()
    {
        if (text)
            text.text = entity.GetSkill(skill).ModifiedLevel.ToString("n0");
    }
    public void AddLvl()
    {
        if (player.levelUpPoints > 0)
        {
            entity.GetSkill(skill).IncreaseLevel(levelToGain);
            player.levelUpPoints -= 1;
            UpdateText();
        }
    }
}
