﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Armor : Equipt
{


    public SkillModifierObject defence;
    private void Start()
    {
        rect = GetComponent<RectTransform>();
    }
    public override void Action()
    {
        if (transform.parent == i_slot)
        {
            rect.SetParent(e_slot);
            rect.localPosition = Vector3.zero;
            if (character && skill)
            {
                //character.skillReference.GetSkill(skill).Level.AddModifier(this);
                character.skillReference.GetSkill(skill).Attributes[0].value.AddModifier(defence);


            }
        }
        else if (transform.parent == e_slot)
        {
            rect.SetParent(i_slot);
            rect.localPosition = Vector3.zero;
            if (character && skill)
            {
                //character.skillReference.GetSkill(skill).Level.RemoveModifier(this);
                character.skillReference.GetSkill(skill).Attributes[0].value.RemoveModifier(defence);

            }
        }

    }
}
