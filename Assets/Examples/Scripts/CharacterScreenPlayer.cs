﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterScreenPlayer : MonoBehaviour
{
    public int maxDamage;
    public int minDamge;

    public int baseDefence;

    public int maxMana;

    public int maxhealth;
    public int maxStamina;

    public int levelUpPoints;

    public Text hpText;
    public Text staminaText;
    public Text maxDamageText;
    public Text minDamageText;
    public Text defenceText;
    public Text maxManaText;
    public Text pointsText;

    public Text levelText;
    public Text xpText;
    SkillReference skillRef;
    private void Start()
    {
        skillRef = GetComponent<SkillController>().skillReference;
        for (int i = 0; i < skillRef.Skills.Count; i++)
        {
            //if (skillRef.Skills[i].skillType.attributes.Length > 0)
            SetValue(skillRef.Skills[i]);

            skillRef.Skills[i].Level.RegisterModEvent(OnSkillModified);
            // if (skillRef.Skills[i].SkillType.name == "Dexterity")
            // {
            for (int a = 0; a < skillRef.Skills[i].Attributes.Count; a++)
            {
                if (skillRef.Skills[i].Attributes[a] != null)
                    skillRef.Skills[i].Attributes[a].value.RegisterModEvent(OnAttributeModified);
            }
           // }

        }
        SetXp(skillRef.Skills[0], 0);

    }

    public void LevelUp(Skill skill)
    {
        SetValue(skill);
        if (skill.SkillType.name == "CharacterLevel")
            levelUpPoints += 1;
    }
    private void Update()
    {

        pointsText.text = levelUpPoints.ToString("n0");
    }
    void OnAttributeModified()
    {
        for (int i = 0; i < skillRef.Skills.Count; i++)
        {
            SetValue(skillRef.Skills[i]);
            //if (skillRef.Skills[i].SkillType.name == "Dexterity")
            //{
            //    baseDefence = skillRef.Skills[i].Attributes[0].value.ModifiedValue;

            //    defenceText.text = baseDefence.ToString("n0");
            //}
        }
    }
    void OnSkillModified()
    {
        for (int i = 0; i < skillRef.Skills.Count; i++)
        {
            skillRef.Skills[i].UpdateAttributes();
            SetValue(skillRef.Skills[i]);
        }
    }
    public void SetValue(Skill skill)
    {
        switch (skill.SkillType.name)
        {
            case "Strength":
                minDamge = skill.Attributes[0].value.ModifiedValue;
                maxDamage = skill.Attributes[1].value.ModifiedValue;

                minDamageText.text = minDamge.ToString("n0");
                maxDamageText.text = maxDamage.ToString("n0");
                break;
            case "Dexterity":
                baseDefence = skill.Attributes[0].value.ModifiedValue;

                defenceText.text = baseDefence.ToString("n0");
                break;
            case "Vitality":
                maxhealth = skill.Attributes[0].value.ModifiedValue;
                maxStamina = skill.Attributes[1].value.ModifiedValue;

                hpText.text = maxhealth.ToString("n0");
                staminaText.text = maxStamina.ToString("n0");
                break;
            case "Energy":
                maxMana = skill.Attributes[0].value.ModifiedValue;

                maxManaText.text = maxMana.ToString("n0");
                break;
            case "CharacterLevel":
                levelText.text = skill.ModifiedLevel.ToString("n0");
                break;
        }
    }
    public void SetXp(Skill skill, int amount)
    {
        if (skill.SkillType.name == "CharacterLevel")
            xpText.text = skill.Experience.ToString("n0");
    }
}

//maxhealth = skillRef.GetSkillAttributeValue(vitality, hp);
// maxStamina = skillRef.GetSkillAttributeValue(vitality, stamina);

//  maxhealth = skillRef.GetSkillAttributeValue(vitality, 0);
//  maxStamina = skillRef.GetSkillAttributeValue(vitality, 1);

// hp.GetValue(skillRef.GetSkill(vitality).Level, ref maxhealth);
// stamina.GetValue(skillRef.GetSkill(vitality).Level, ref maxStamina);