﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Equipt : MonoBehaviour
{
    public RectTransform i_slot;
    public RectTransform e_slot;
    protected  RectTransform rect;

    public SkillController character;
    public SkillObject skill;
   // public Modifier modifier;

    private void Start()
    {
        rect = GetComponent<RectTransform>();
    }
    public abstract void Action();
}
