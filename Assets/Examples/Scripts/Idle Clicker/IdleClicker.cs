﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IdleClicker : MonoBehaviour
{
    public SkillListObject entity;
    public SkillObject skill;

    public Text goldText;
    public Text levelText;
    public Text levelRequiredText;
    public Text goldRequiredText;
    public Text expGainedText;
    public Text goldGainedText;
    int gold = 0;
    int requiredLevel = 10;
    int requiredGold = 100;
    int expToGain = 5;
    int goldToGain = 4;
    //readonly Modifier mod = new Modifier("TestMod", 10, 0, true, 1);
    private void Start()
    {
        UpdateText();
    }


    public void Action()
    {
        entity.GetSkill(skill).AddSkillXP(expToGain);

        gold += goldToGain;
    }
  
    public void OnLevelUp()
    {
        gold += (int)((entity.GetSkill(skill).ModifiedLevel / 5) * goldToGain);
    }

    public void AdjustExpToGain(int value)
    {
        expToGain = value;
    }
    public void AdjustGoldToGain(int value)
    {
        goldToGain = value;
    }
    public void Upgrade()
    {
        if (entity.GetSkill(skill).ModifiedLevel >= requiredLevel && gold >= requiredGold)
        {
            gold -= requiredGold;

            requiredLevel = requiredLevel * 2;
            requiredGold = (requiredGold * 2) * (int)(entity.GetSkill(skill).ModifiedLevel / 2);
            expToGain = expToGain * 2;
            goldToGain = goldToGain * 8;
        }
    }
    private void Update()
    {
        UpdateText();
    }
    public void UpdateText()
    {
        levelText.text = entity.GetSkill(skill).ModifiedLevel.ToString("n0");
        goldText.text = gold.ToString("n0");
        levelRequiredText.text = requiredLevel.ToString("n0");
        goldRequiredText.text = requiredGold.ToString("n0");
        expGainedText.text = expToGain.ToString("n0");
        goldGainedText.text = goldToGain.ToString("n0");
    }

}
