﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Skill Attribute", menuName = "Exp System/New LOL Skill Attribute")]
public class LOLattributeGrowth : SkillAttributeObject {
    public float baseValue;
    public float growthStatistic;
    public override void GetValue(int level, ref int value)
    {
        value = (int)System.Math.Ceiling(baseValue + growthStatistic * (level - 1) * (0.7025 + 0.0175 * (level - 1)));
    }
}
