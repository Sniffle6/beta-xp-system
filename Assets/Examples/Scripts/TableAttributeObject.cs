﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Skill Attribute", menuName = "Exp System/New Skill Attribute")]
public class TableAttributeObject : SkillAttributeObject
{
    public List<Table> table = new List<Table>();
    public override void GetValue(int level, ref int value)
    {
        for (int i = 0; i < table.Count; i++)
        {
            if (table[i].minLevel <= level && table[i].maxLevel >= level)
            {
                value = table[i].value;
                break;
            }
        }
        if (level > table[table.Count - 1].maxLevel)
            value = table[table.Count - 1].value;
    }
}
[System.Serializable]
public class Table
{
    public int minLevel;
    public int maxLevel;
    public int value;
}