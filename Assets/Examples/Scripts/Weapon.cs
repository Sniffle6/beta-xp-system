﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : Equipt
{


    public SkillModifier minDamage;
    public SkillModifier maxDamage;
    private void Start()
    {
        rect = GetComponent<RectTransform>();
    }
    public override void Action()
    {
        if (transform.parent == i_slot)
        {
            rect.SetParent(e_slot);
            rect.localPosition = Vector3.zero;
            if (character && skill)
            {
                character.skillReference.GetSkill(skill).Attributes[0].value.AddModifier(minDamage);
                character.skillReference.GetSkill(skill).Attributes[1].value.AddModifier(maxDamage);
            }
        }
        else if (transform.parent == e_slot)
        {
            rect.SetParent(i_slot);
            rect.localPosition = Vector3.zero;
            if (character && skill)
            {
                //character.skillReference.GetSkill(skill).Level.RemoveModifier(this);
                character.skillReference.GetSkill(skill).Attributes[0].value.RemoveModifier(minDamage);
                character.skillReference.GetSkill(skill).Attributes[1].value.RemoveModifier(maxDamage);
            }
        }

    }
    
}
[System.Serializable]
public class SkillModifier : IModifiers
{
    public bool isUpdateable;
    public int value;
    public void AddValue(ref int v)
    {
        v += value;
       // if(isUpdateable)
             
            
    }

    public IEnumerator OnUpdate()
    {
        throw new System.NotImplementedException();
    }
}