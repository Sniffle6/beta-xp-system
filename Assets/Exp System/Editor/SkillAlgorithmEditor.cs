﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SkillAlgorithmObject))]
public class SkillAlgorithmEditor : Editor
{
    SkillAlgorithmObject m_Target;
    int currentlyDisplayedDetails;
    Vector2 scrollPos;
    float horizontalPosition;
    AnimationCurve curve = AnimationCurve.Linear(0, 0, 10, 10);
    int NUMBER_OF_COLUMNS;
    Keyframe[] keys;
    GUIStyle buttonStyle;
    public bool ShowDetails = true;

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        m_Target = (SkillAlgorithmObject)target;
        if (buttonStyle == null || buttonStyle.fontSize != m_Target.buttonSize / 7)
        {
            buttonStyle = new GUIStyle(GUI.skin.GetStyle("Box"))
            {
                fontSize = m_Target.buttonSize / 7,
                alignment = TextAnchor.MiddleCenter
            };
        }
        DrawAlgorithmDetails();
    }
    void DrawAlgorithmDetails()
    {
        Rect curveRect = new Rect(EditorGUIUtility.singleLineHeight - 1, EditorGUIUtility.singleLineHeight * 10, Screen.width - 37, 200);
        //EditorGUI.BeginChangeCheck();
        m_Target.firstPassAmplifier = Mathf.Abs(EditorGUILayout.IntField("First Pass Amplifier", (int)m_Target.firstPassAmplifier));
        EditorGUI.BeginChangeCheck();
        m_Target.firstPassExponentBase = Mathf.Abs(EditorGUILayout.IntField("First Pass Exponent Base", (int)m_Target.firstPassExponentBase));//EditorGUILayout.IntSlider(new GUIContent("First Pass Exponent Base"), (int)m_Target.firstPassExponentBase, 1, 100);
        if (EditorGUI.EndChangeCheck())
        {
            for (int i = 1; i <= m_Target._maxLevel - 1; i++)
            {
                if (m_Target.GetXPForLevel(i) <= m_Target.GetXPForLevel(i - 1))
                    m_Target.firstPassDividerForLevelExponent += 1f;
            }
        }
        EditorGUI.BeginChangeCheck();
        m_Target.firstPassDividerForLevelExponent = Mathf.Abs(EditorGUILayout.IntField("Frst Pass Divider For Level Exponent", (int)m_Target.firstPassDividerForLevelExponent));//EditorGUILayout.IntSlider(new GUIContent("Frst Pass Divider For Level Exponent"), (int)m_Target.firstPassDividerForLevelExponent, 1, 150);
        if (EditorGUI.EndChangeCheck())
        {
            for (int i = 1; i <= m_Target._maxLevel - 1; i++)
            {
                if (m_Target.GetXPForLevel(i) <= m_Target.GetXPForLevel(i - 1))
                    m_Target.firstPassExponentBase -= 1f;
            }
        }
        m_Target.secondPassDivider = Mathf.Abs(EditorGUILayout.IntField("Second Pass Divider", (int)m_Target.secondPassDivider));//EditorGUILayout.IntSlider(new GUIContent("Second Pass Divider"), (int)m_Target.secondPassDivider, 1, 20);
        curve = EditorGUI.CurveField(curveRect, "Level Up graph", curve);
        if (keys == null)
            keys = curve.keys;
        if (keys.Length != m_Target._maxLevel)
            System.Array.Resize(ref keys, m_Target._maxLevel);
        for (int i = 0; i < keys.Length; i++)
        {
            //keys[i].weightedMode = WeightedMode.Both;
            //keys[i].inTangent = m_Target.getXPForLevel(i) - m_Target.getXPForLevel(i - 1);
            // keys[i].outTangent = m_Target.getXPForLevel(i + 1) - m_Target.getXPForLevel(i);
            keys[i].time = i + 1;
            keys[i].value = m_Target.GetXPForLevel(i + 1);
        }
        curve.keys = keys;
        curve.AddKey(new Keyframe(keys[keys.Length - 1].time, keys[keys.Length - 1].value));
        for (int i = 0; i < keys.Length; i++)
        {
            AnimationUtility.SetKeyLeftTangentMode(curve, i, AnimationUtility.TangentMode.Constant);
            AnimationUtility.SetKeyRightTangentMode(curve, i, AnimationUtility.TangentMode.Constant);
        }
        ShowDetails = EditorGUI.Foldout(new Rect(EditorGUIUtility.singleLineHeight - 1, EditorGUIUtility.singleLineHeight * 11, Screen.width - 85, EditorGUIUtility.singleLineHeight), ShowDetails, "Details");
        if (ShowDetails)
        {
            //int.TryParse(EditorGUI.TextField(new Rect(EditorGUIUtility.singleLineHeight - 1, EditorGUIUtility.singleLineHeight * 23, Screen.width - 37, EditorGUIUtility.singleLineHeight), new GUIContent("Button Size"), m_Target.buttonSize.ToString("n0")), out m_Target.buttonSize);
            //if (m_Target.buttonSize == 0)
            //    m_Target.buttonSize = 1;
            m_Target.buttonSize = EditorGUI.IntSlider(new Rect(EditorGUIUtility.singleLineHeight - 1, EditorGUIUtility.singleLineHeight * 23, Screen.width - 37, EditorGUIUtility.singleLineHeight), new GUIContent("Button Size"), m_Target.buttonSize, 0, 200);
            EditorGUI.LabelField(new Rect(EditorGUIUtility.singleLineHeight - 1, EditorGUIUtility.singleLineHeight * 24, Screen.width - 85, EditorGUIUtility.singleLineHeight), GetLevelDetails(currentlyDisplayedDetails));
            EditorGUI.LabelField(new Rect(EditorGUIUtility.singleLineHeight - 1, 280, Screen.width / 3, EditorGUIUtility.singleLineHeight), "Min Level Color");
            m_Target.minLevelColor = EditorGUI.ColorField(new Rect(EditorGUIUtility.singleLineHeight + 1, 300, Screen.width / 3, EditorGUIUtility.singleLineHeight), GUIContent.none, m_Target.minLevelColor, true, true, true);
            EditorGUI.LabelField(new Rect(EditorGUIUtility.singleLineHeight - 1, 320, Screen.width / 3, EditorGUIUtility.singleLineHeight), "Max Level Color");
            m_Target.maxLevelColor = EditorGUI.ColorField(new Rect(EditorGUIUtility.singleLineHeight + 1, 340, Screen.width / 3, EditorGUIUtility.singleLineHeight), GUIContent.none, m_Target.maxLevelColor, true, true, true);
            if (m_Target.buttonSize != 0)
            {
                GUILayout.BeginHorizontal();
                {
                    float maxLevelXP = (float)m_Target.GetXPForLevel(m_Target._maxLevel);
                    for (int i = 1; i <= m_Target._maxLevel - 1; i++)
                    {
                        float value = m_Target.GetXPForLevel(i) / maxLevelXP;
                        GUI.backgroundColor = GetColorValue(value);
                        if (GUI.Button(new Rect((EditorGUIUtility.singleLineHeight - 1) + GetXSpacing(i), 400 + GetYSpacing(i), m_Target.buttonSize, m_Target.buttonSize / 2), new GUIContent(GetLevelDetails(i), GetLevelDetails(i)), buttonStyle))
                            currentlyDisplayedDetails = i;
                    }
                }
                scrollPos = EditorGUILayout.BeginScrollView(scrollPos, GUILayout.Width(Screen.width - 100), GUILayout.Height((250 + m_Target.buttonSize) + GetYSpacing(m_Target._maxLevel)));
                EditorGUILayout.EndScrollView();
                GUILayout.EndHorizontal();
            }
        }
        //EditorGUI.EndChangeCheck();
    }
    private float GetXSpacing(int value)
    {
        return (m_Target.buttonSize * ((value - 1) % ((Screen.width - (m_Target.buttonSize / 2)) / m_Target.buttonSize)));
    }
    private float GetYSpacing(int value)
    {
        return ((m_Target.buttonSize * ((value - 1) / ((Screen.width - (m_Target.buttonSize / 2)) / m_Target.buttonSize))) / 2);
    }
    private string GetLevelDetails(int value)
    {
        return string.Concat("L:", value + 1, " Xp: ", m_Target.GetXPForLevel(value).ToString("n0"));//"L:" + value + " Xp: " + m_Target.getXPForLevel(value).ToString("n0");
    }
    private Color GetColorValue(float value)
    {
        return Color.Lerp(m_Target.maxLevelColor, m_Target.minLevelColor, 1 - value);
    }
}
