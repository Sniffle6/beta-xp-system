﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(SkillController))]
[CanEditMultipleObjects]
public class SkillControllerEditor : Editor
{
    SkillController m_Target;
    bool showDetails = true;
    bool showXP = true;
    string xpToAdd = "100";
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        m_Target = (SkillController)target;


        if (m_Target.skillReference.GetStats() != null)
        {
            bool shouldReturn = false;
            if (!m_Target.skillReference.levelUpAlgorithm && m_Target.skillReference.storeDataLocally)
            {
                DrawHelpBox("Level up Algorithm not set!.");
                shouldReturn = true;
            }
            if (m_Target.skillReference.GetStats().List.Count == 0)
            {
                DrawHelpBox("No skills set");
                shouldReturn = true;
            }
            for (int i = 0; i < m_Target.skillReference.GetStats().List.Count; i++)
            {
                if (m_Target.skillReference.GetStats().List[i] == null)
                {
                    DrawHelpBox("Skill[" + i + "] is not set! Please select a skill.");
                    shouldReturn = true;
                }
            }
            if (shouldReturn)
                return;
            showDetails = EditorGUILayout.Foldout(showDetails, "Skill Details");
            if (showDetails)
                for (int i = 0; i < m_Target.skillReference.GetStats().List.Count; i++)
                {
                    DrawSkillDetail(i);
                }
            showXP = EditorGUILayout.Foldout(showXP, "'Add Xp' Buttons");
            if (showXP)
            {
                xpToAdd = EditorGUILayout.TextField("Xp to add: ", xpToAdd);
                for (int i = 0; i < m_Target.skillReference.GetStats().List.Count; i++)
                {
                    DrawAddXpButton(i);
                }


            }
        }
        else
            DrawHelpBox("No skill list selected!");
    }
    void DrawHelpBox(string message)
    {
        EditorGUILayout.HelpBox(message, MessageType.Error);
    }
    void DrawAddXpButton(int index)
    {
        if (index < 0 || index >= m_Target.skillReference.GetStats().List.Count)
            return;
        if (m_Target.skillReference.GetStats().List[index].skillType)
            if (GUILayout.Button("Add XP To Skill " + m_Target.skillReference.GetStats().List[index].skillType.name))
            {
                m_Target.GetSkills[index].AddSkillXP(int.Parse(xpToAdd));
            }
    }

    void DrawSkillDetail(int index)
    {
        if (index < 0 || index >= m_Target.skillReference.GetStats().List.Count)
            return;
        GUILayout.BeginHorizontal();
        {
            if (m_Target.skillReference.Skills.Count > index)
            {
                EditorGUILayout.LabelField("Skill " + index + ":", m_Target.skillReference.GetStats().List[index].skillType.name + ", Level: " + m_Target.GetSkills[index].Level.ModifiedValue + ", Exp: " + m_Target.GetSkills[index].Experience);
            }
            else
            {
                if (m_Target.skillReference.GetStats().List[index].skillType)
                    EditorGUILayout.LabelField("Skill " + index + ":", m_Target.skillReference.GetStats().List[index].skillType.name);
            }
        }
        GUILayout.EndHorizontal();
    }

}
