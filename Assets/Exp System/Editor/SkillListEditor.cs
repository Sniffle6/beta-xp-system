﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEditorInternal;
using UnityEditor;
using UnityEngine;
using System.Linq;

[CustomPropertyDrawer(typeof(SimpleReorderableList), true)]
public class SkillListEditor : PropertyDrawer
{
    private ReorderableList list;

    private ReorderableList GetList(SerializedProperty property)
    {
        if (list == null)
        {
            var heights = new float[property.arraySize];
            bool[] ShowAttributes = new bool[property.arraySize];
            list = new ReorderableList(property.serializedObject, property, true, true, true, true)
            {
                drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
                {
                    rect.width -= 40;
                    rect.x += 20;
                    var element = list.serializedProperty.GetArrayElementAtIndex(index);
                    var attribute = element.FindPropertyRelative("attributes");
                    rect.y += 2;

                    EditorGUI.BeginChangeCheck();
                    EditorGUI.PropertyField(
                        new Rect(rect.x, rect.y, 120, EditorGUIUtility.singleLineHeight),
                        element.FindPropertyRelative("skillType"), GUIContent.none);

                    //var obj = element.FindPropertyRelative("skillType").objectReferenceValue as SkillObject;
                    //obj = (SkillObject)EditorGUI.ObjectField(new Rect(rect.x, rect.y, 120, EditorGUIUtility.singleLineHeight), obj, typeof(SkillObject), true);
                    if (EditorGUI.EndChangeCheck())
                    {
                       // var prefab = element.FindPropertyRelative("skillType").objectReferenceValue as SkillObject;
                      //  EditorGUIUtility.PingObject(prefab);
                        element.serializedObject.ApplyModifiedProperties();
                    }


                    ShowAttributes[index] = EditorGUI.Foldout(new Rect(rect.x + 130, rect.y, 120, EditorGUIUtility.singleLineHeight), ShowAttributes[index], "Attributes");
                    if (ShowAttributes[index])
                    {
                        for (int i = 0; i < attribute.arraySize; i++)
                        {
                            DrawAttribute(element, rect, i, index);
                        }
                        DrawAddStateButton(rect, attribute);
                    }

                    float arraySize = attribute.arraySize;

                    float height = EditorGUIUtility.singleLineHeight*1.25f;
                    bool foldout = ShowAttributes[index];
                    if (foldout)
                    {
                        height = (EditorGUIUtility.singleLineHeight * 2f) + ((arraySize + 0.5f) * EditorGUIUtility.singleLineHeight);
                    }

                    try
                    {
                        heights[index] = height;
                    }
                    catch (ArgumentOutOfRangeException e)
                    {
                        Debug.LogWarning(e.Message);
                    }
                    finally
                    {
                        float[] floats = heights.ToArray();
                        Array.Resize(ref floats, list.serializedProperty.arraySize);
                        heights = floats;
                    }
                },

                onAddCallback = (ReorderableList l) =>
                {
                    var index = l.serializedProperty.arraySize;
                    l.serializedProperty.arraySize++;
                    l.index = index;

                    float[] floats = heights.ToArray();
                    Array.Resize(ref floats, list.serializedProperty.arraySize);
                    heights = floats;


                    Array.Resize(ref ShowAttributes, list.serializedProperty.arraySize);
                },

                elementHeightCallback = (index) =>
                {
                    float height = 0;

                    try
                    {
                        height = heights[index];
                    }
                    catch (ArgumentOutOfRangeException e)
                    {
                        Debug.LogWarning(e.Message);
                    }
                    finally
                    {
                        float[] floats = heights.ToArray();
                        Array.Resize(ref floats, list.serializedProperty.arraySize);
                        heights = floats;
                    }
                    return height;
                },

                drawHeaderCallback = (Rect rect) =>
                {
                    EditorGUI.LabelField(rect, "Entity Skills");
                }

            };
        }
        return list;
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return GetList(property.FindPropertyRelative("List")).GetHeight();
    }
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        SerializedProperty listProperty = property.FindPropertyRelative("List");
        var list = GetList(listProperty);
        list.DoList(position);
    }

    private void DrawAttribute(SerializedProperty serializedProperty, Rect rect, int index, int parentIndex)
    {
        if (serializedProperty == null)
        {
            return;
        }
        EditorGUI.BeginChangeCheck();
        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel += 10;
        //serializedProperty.FindPropertyRelative("attributes").objectReferenceValue
        //EditorGUI.ObjectField(new Rect(rect.x, rect.y + ((index + 1) * EditorGUIUtility.singleLineHeight), rect.width - 35, EditorGUIUtility.singleLineHeight), serializedProperty.FindPropertyRelative("attributes").GetArrayElementAtIndex(index), new GUIContent("Attribute: " + index));
        // serializedProperty.FindPropertyRelative("attributes").objectReferenceValue
        //var obj = serializedProperty.FindPropertyRelative("attributes").GetArrayElementAtIndex(index).objectReferenceValue as SkillAttributeObject;
        //obj = (SkillAttributeObject)EditorGUI.ObjectField(new Rect(rect.x, rect.y + ((index + 1) * EditorGUIUtility.singleLineHeight), rect.width - 35, EditorGUIUtility.singleLineHeight), obj, typeof(SkillAttributeObject), false);

        
        EditorGUI.PropertyField(
            new Rect(rect.x, rect.y + ((index + 1) * EditorGUIUtility.singleLineHeight), rect.width - 35, EditorGUIUtility.singleLineHeight),
            serializedProperty.FindPropertyRelative("attributes").GetArrayElementAtIndex(index), GUIContent.none);



        EditorGUI.indentLevel = indent;
        if (EditorGUI.EndChangeCheck())
        {
           // var prefab = serializedProperty.FindPropertyRelative("attributes").GetArrayElementAtIndex(index).objectReferenceValue as SkillAttributeObject;
           // EditorGUIUtility.PingObject(prefab);
            serializedProperty.FindPropertyRelative("attributes").GetArrayElementAtIndex(index).serializedObject.ApplyModifiedProperties();
            //Undo.RecordObject(m_Target, "Modify State");
            //m_Target.States[index].Name = newName;
            //m_Target.States[index].Position = newPosition;
            //EditorUtility.SetDirty(m_Target);
        }

        if (GUI.Button(new Rect(rect.x + (rect.width - 25), rect.y + ((index + 1) * EditorGUIUtility.singleLineHeight), 25, EditorGUIUtility.singleLineHeight), "R"))
        {
            EditorApplication.Beep();

            if (EditorUtility.DisplayDialog("Really?", "Do you really want to remove the attribute on skill " + serializedProperty.FindPropertyRelative("skillType").name + " at position " + index + "?", "Yes", "No") == true)
            {
                Undo.RecordObject(serializedProperty.serializedObject.targetObject, "Delete Attribute");
                EditorUtility.SetDirty(serializedProperty.serializedObject.targetObject);

                serializedProperty.FindPropertyRelative("attributes").DeleteArrayElementAtIndex(index);
            }
        }
    }

    void DrawAddStateButton(Rect rect, SerializedProperty property)
    {
        if (GUI.Button(new Rect(rect.x + (rect.width - ((rect.width / 4) + 52)), rect.y + ((property.arraySize + 1) * EditorGUIUtility.singleLineHeight), rect.width / 4, EditorGUIUtility.singleLineHeight), "Add Attribute"))
        {
            Undo.RecordObject(property.serializedObject.targetObject, "Add Attribute");
            EditorUtility.SetDirty(property.serializedObject.targetObject);

            property.InsertArrayElementAtIndex(property.arraySize);
        }
    }
}


//m_Target = (SkillListObject)target;
//list = new ReorderableList(serializedObject,
//        serializedObject.FindProperty("_stat"),
//        true, true, true, true);

//SerializedProperty prop = serializedObject.FindProperty("_stat");

//ShowAttributes = new bool[prop.arraySize];
//List<float> heights = new List<float>(prop.arraySize);

//list.drawElementCallback =
//(Rect rect, int index, bool isActive, bool isFocused) =>
//{
//    var element = list.serializedProperty.GetArrayElementAtIndex(index);
//    rect.y += 2;

//    EditorGUI.PropertyField(
//        new Rect(rect.x, rect.y, 120, EditorGUIUtility.singleLineHeight),
//        element.FindPropertyRelative("skillType"), GUIContent.none);

//    //EditorGUI.PropertyField(
//    //    new Rect(rect.x + 135, rect.y, rect.width + 120, EditorGUIUtility.singleLineHeight * 10),
//    //    element.FindPropertyRelative("attributes"), GUIContent.none, true);

//    ShowAttributes[index] = EditorGUI.Foldout(new Rect(rect.x+130, rect.y, 120, EditorGUIUtility.singleLineHeight), ShowAttributes[index], "Attributes");
//    if (ShowAttributes[index])
//    {
//        for (int i = 0; i < element.FindPropertyRelative("attributes").arraySize; i++)
//        {
//            DrawAttribute(element.FindPropertyRelative("attributes").GetArrayElementAtIndex(i), rect, i, index);
//        }
//        DrawAddStateButton(rect, index, element.FindPropertyRelative("attributes").arraySize);
//    }
//    bool foldout = ShowAttributes[index];

//    float arraySize = 1.25f;
//    if (element.FindPropertyRelative("attributes").arraySize != 0)
//    {
//        arraySize = element.FindPropertyRelative("attributes").arraySize;
//    }

//    float height = EditorGUIUtility.singleLineHeight * 1.25f;

//    if (foldout)
//    {
//        height = (EditorGUIUtility.singleLineHeight * 2f) + ((arraySize+0.5f)* EditorGUIUtility.singleLineHeight) ;
//    }

//    try
//    {
//        heights[index] = height;
//    }
//    catch (ArgumentOutOfRangeException e)
//    {
//        Debug.LogWarning(e.Message);
//    }
//    finally
//    {
//        float[] floats = heights.ToArray();
//        Array.Resize(ref floats, prop.arraySize);
//        heights = floats.ToList();
//    }
//};

//list.drawHeaderCallback = (Rect rect) =>
//{
//    EditorGUI.LabelField(rect, "Entity Skills");
//};

//list.onAddCallback = (ReorderableList l) => {
//    var index = l.serializedProperty.arraySize;
//    l.serializedProperty.arraySize++;
//    l.index = index;
//    var element = l.serializedProperty.GetArrayElementAtIndex(index);
//    float height = EditorGUIUtility.singleLineHeight * 1.25f;

//    float[] floats = heights.ToArray();
//    Array.Resize(ref floats, prop.arraySize);
//    heights = floats.ToList();


//    Array.Resize(ref ShowAttributes, prop.arraySize);
//};

//list.elementHeightCallback = (index) =>
//{
//    //Repaint();
//    float height = 0;

//    try
//    {
//        height = heights[index];
//    }
//    catch (ArgumentOutOfRangeException e)
//    {
//        Debug.LogWarning(e.Message);
//    }
//    finally
//    {
//        float[] floats = heights.ToArray();
//        Array.Resize(ref floats, prop.arraySize);
//        heights = floats.ToList();
//    }

//    return height;
//};