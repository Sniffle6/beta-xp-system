﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(SkillReference))]
public class SkillReferenceDrawer : PropertyDrawer
{
    /// <summary>
    /// Options to display in the popup to select constant or variable.
    /// </summary>
    private readonly string[] popupOptions =
        { "Use Constant", "Use Variable" };

    /// <summary> Cached style to use to draw the popup button. </summary>
    private GUIStyle popupStyle;


    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {

        if (popupStyle == null)
        {
            popupStyle = new GUIStyle(GUI.skin.GetStyle("PaneOptions"))
            {
                imagePosition = ImagePosition.ImageOnly

            };
        }


        label = EditorGUI.BeginProperty(position, label, property);
        position = EditorGUI.PrefixLabel(position, label);

        EditorGUI.BeginChangeCheck();

        // Get properties
        SerializedProperty useConstant = property.FindPropertyRelative("storeDataLocally");
        SerializedProperty levAlgorithm = property.FindPropertyRelative("levelUpAlgorithm");
        SerializedProperty constantValue = property.FindPropertyRelative("Stats");
        SerializedProperty variable = property.FindPropertyRelative("skillObjects");
        // Calculate rect for configuration button

        Rect buttonRect = new Rect(position.position, new Vector2(EditorGUIUtility.singleLineHeight, EditorGUIUtility.singleLineHeight));
        buttonRect.yMin += popupStyle.margin.top;
        buttonRect.width = popupStyle.fixedWidth + popupStyle.margin.right;
        position.xMin = buttonRect.xMax;

        Rect algorithmRect = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);
        Rect listRect = new Rect(new Vector2(EditorGUIUtility.singleLineHeight, position.y + EditorGUIUtility.singleLineHeight), position.size + new Vector2(Screen.width - (position.size.x+36), 0));

        // Store old indent level and set it to 0, the PrefixLabel takes care of it
        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;
        int result = EditorGUI.Popup(buttonRect, useConstant.boolValue ? 0 : 1, popupOptions, popupStyle);

        useConstant.boolValue = result == 0;

        if (useConstant.boolValue)
        {
            EditorGUI.PropertyField(algorithmRect, levAlgorithm, GUIContent.none);
            EditorGUI.PropertyField(listRect, constantValue, new GUIContent("Skills: "), true);
        }
        else
        {
            EditorGUI.PropertyField(position, variable, GUIContent.none, false);
        }


        if (EditorGUI.EndChangeCheck())
            property.serializedObject.ApplyModifiedProperties();

        EditorGUI.indentLevel = indent;
        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        float baseSize = 75;
        //Debug.Log(EditorGUI.GetPropertyHeight(property, label, true));
        //if (property.FindPropertyRelative("storeDataLocally").boolValue)
        //    return EditorGUI.GetPropertyHeight(property, label, true);
        //return EditorGUIUtility.singleLineHeight;
        if (property.FindPropertyRelative("storeDataLocally").boolValue)
        {
            var value = property.FindPropertyRelative("Stats");
            //var list = value.FindPropertyRelative("List");
            //var attributeSize = 0.0f;
            //for (int i = 0; i < list.arraySize; i++)
            //{
            //    attributeSize += list.GetArrayElementAtIndex(i).FindPropertyRelative("attributes").arraySize;
            //}
            //float size = 0;
            //size = baseSize + (40*list.arraySize) + (attributeSize*EditorGUIUtility.singleLineHeight);

            return EditorGUI.GetPropertyHeight(value)+EditorGUIUtility.singleLineHeight;
        }
        else
            return EditorGUIUtility.singleLineHeight;

    }
}
