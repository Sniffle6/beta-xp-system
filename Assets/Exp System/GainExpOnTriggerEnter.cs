﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GainExpOnTriggerEnter : MonoBehaviour
{
    public int _expToGain;
    public SkillObject _skillToGiveExpToo;

    private void OnTriggerEnter(Collider other)
    {
        print("Entered trigger");
        SkillReference entitySkills = null;
        if (other.GetComponent<SkillController>())
            entitySkills = other.GetComponent<SkillController>().skillReference;

        entitySkills.GetSkill(_skillToGiveExpToo).AddSkillXP(_expToGain);

    }
}