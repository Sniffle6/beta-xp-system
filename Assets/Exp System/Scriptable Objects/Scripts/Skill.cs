﻿using System;
using System.Collections.Generic;
using UnityEngine;
//using System.Linq;
using System.Collections;

[Serializable]
public class Skill
{
    public SkillObject SkillType { get; private set; }
    public List<SkillAttribute> Attributes { get; private set; }
    [SerializeField]
    private int _experience;
    private ModifiableInt _level = new ModifiableInt();
    public SkillAlgorithmObject Algorith { get; private set; }
    [HideInInspector]
    public SkillController Controller;
    public ModifiableInt Level
    {
        get { if (_level != null) { return _level; } else { return _level = new ModifiableInt(); }; }
    }
    public int ModifiedLevel
    {
        get { return _level.ModifiedValue; }
    }
    public int BaseLevel
    {
        get { return _level.BaseValue; }
    }
    public int Experience
    {
        get { return _experience; }
    }
    public Action<Skill> OnLevelUp;
    public Action<Skill, int> OnXpGained;
    private const int MAX_EXP = 2147000000;
    public Skill(SkillController controller, SkillObject skillType, List<SkillAttributeObject> attributes, int level, SkillAlgorithmObject algorithm, Action<Skill> onLevUp, Action<Skill, int> onXpGained)
    {
        Controller = controller;
        Algorith = algorithm;
        SkillType = skillType;
        _level.BaseValue = level;
        _experience = GetXPForLevel(level - 1);
        OnLevelUp = onLevUp;
        OnXpGained = onXpGained;
        Attributes = new List<SkillAttribute>();
        for (int i = 0; i < attributes.Count; i++)
        {
            Attributes.Add( new SkillAttribute
            {
                AttributeType = attributes[i]
            });
            int value = Attributes[i].value.BaseValue;
            Debug.Log(attributes.Count);
            attributes[i].GetValue(Level.ModifiedValue, ref value);
            Attributes[i].value.BaseValue = value;
        }
        //Attributes = attributes;
    }
    public int GetXPForLevel(int level)
    {
        return Algorith.GetXPForLevel(level);
    }
    public int GetLevelForXP(int exp)
    {
        return Algorith.GetLevelForXP(exp);
    }
    public SkillAttribute GetAttribute(string attribute)
    {
        for (int i = 0; i < Attributes.Count; i++)
        {
            if (Attributes[i].AttributeType.name == attribute)
                return Attributes[i];
        }
        return null;
    }
    public int IncreaseLevel(int value)
    {
        var newXp = GetXPForLevel(BaseLevel) + 1;
        _experience = newXp;
        LevelUp(_level.BaseValue);
        return BaseLevel;
    }
    /// <summary>
    /// Add skill exp to current skill
    /// </summary>
    /// <param name="amount">The amount of xp to add</param>
    /// <returns>Returns true if you level up</returns>
    public bool AddSkillXP(int amount)
    {
        if (amount + Experience < 0 || Experience > MAX_EXP)
        {
            if (Experience > MAX_EXP)
            {
                _experience = MAX_EXP;
            }
            return false;
        }
        int oldLevel = GetLevelForXP(Experience);
        _experience += amount;
        OnXpGained.Invoke(this, amount);
        return LevelUp(oldLevel);
    }
    public void UpdateAttributes()
    {
        for (int i = 0; i < Attributes.Count; i++)
        {
            int value = Attributes[i].value.BaseValue;
            Attributes[i].AttributeType.GetValue(Level.ModifiedValue, ref value);
            Attributes[i].value.BaseValue = value;
        }

    }
    private bool LevelUp(int oldLevel)
    {
        var newLevel = GetLevelForXP(Experience);
        if (oldLevel < newLevel)
        {
            if (BaseLevel < newLevel)
            {
                _level.BaseValue = newLevel;
                UpdateAttributes();
                if (OnLevelUp != null)
                    OnLevelUp.Invoke(this);
                return true;
            }
        }
        return false;
    }
}
