﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Algorithm", menuName = "Exp System/New Skill Algorithm")]
public class SkillAlgorithmObject : ScriptableObject {
    
    [HideInInspector]
    public float firstPassAmplifier = 300.0f;
    [HideInInspector]
    public float firstPassExponentBase = 2.0f;
    [HideInInspector]
    public float firstPassDividerForLevelExponent = 7.0f;
    [HideInInspector]
    public int secondPassDivider = 4;
    private const int MAX_EXP = 2147000000;
    public int _maxLevel = 99;
    [HideInInspector]
    public int buttonSize = 50;
    [HideInInspector]
    public Color maxLevelColor = Color.white;
    [HideInInspector]
    public Color minLevelColor = Color.white;
    public int GetXPForLevel(int level)
    {
        if (level > _maxLevel)//Todo: and throw an exception dependant of game design
            return 0;

        int firstPassXP = 0;
        int secondPassXP = 0;
        for (int levelCycle = 1; levelCycle <= level; levelCycle++)
        {
            firstPassXP += (int)Math.Floor(levelCycle + (firstPassAmplifier * Math.Pow(firstPassExponentBase, levelCycle / firstPassDividerForLevelExponent)));
            secondPassXP = Mathf.CeilToInt(firstPassXP / secondPassDivider);
        }

        if (secondPassXP > MAX_EXP)
        {
            return MAX_EXP;//Todo: and throw an exception dependant of game design
        }
        if (secondPassXP < 0)
        {
            return MAX_EXP;//Todo: and throw an exception dependant of game design
        }

        return secondPassXP;
    }
    public int GetLevelForXP(int exp)
    {
        int firstPassXP = 0;
        int secondPassXP = 0;
        if (exp > MAX_EXP)
            return _maxLevel;
        for (int levelCycle = 1; levelCycle <= _maxLevel; levelCycle++)
        {
            firstPassXP += (int)Math.Floor(levelCycle + firstPassAmplifier * Math.Pow(firstPassExponentBase, levelCycle / firstPassDividerForLevelExponent));
            secondPassXP = Mathf.CeilToInt(firstPassXP / secondPassDivider);
            if (secondPassXP >= exp)
            {
                return levelCycle;
            }
        }
        return 0;
    }
}
