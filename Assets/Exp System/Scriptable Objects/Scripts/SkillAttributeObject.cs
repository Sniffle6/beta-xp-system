﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SkillAttributeObject : ScriptableObject {

    public abstract void GetValue(int level, ref int value);
}
