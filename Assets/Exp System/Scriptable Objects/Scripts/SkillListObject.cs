﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
[CreateAssetMenu(fileName = "Unamed Skill List", menuName = "Exp System/New Skill List")]
public class SkillListObject : ScriptableObject
{
    public SkillAlgorithmObject _algorithm;
    public ReorderableStat _stat = new ReorderableStat();

    public List<Skill> skills = new List<Skill>();

    public void SetUpSkills(SkillController controller, Action<Skill> levelUp, Action<Skill, int> xpGained)
    {
        for (int i = 0; i < _stat.List.Count; i++)
        {
                
            skills.Add(new Skill(controller, _stat.List[i].skillType, _stat.List[i].attributes, 1, _algorithm, levelUp, xpGained));
        }
    }
    public void RemoveSkills()
    {
        skills.Clear();
    }

    public Skill GetSkill(SkillObject skillToGet)
    {
        foreach (var skill in skills)
        {
            if (skill.SkillType == skillToGet)
                return skill;
        }
        return null;
        //return skills[Array.IndexOf(_stat, skillToGet)];
    }
    public Skill GetSkill(string skillToGet)
    {
        foreach (var skill in skills)
        {
            if (skill.SkillType.name == skillToGet)
                return skill;
        }
        return null;
        //return skills[Array.IndexOf(_stat, skillToGet)];
    }
    public bool HasRequiredLevel(SkillObject skill, int value)
    {
        if (GetSkill(skill).ModifiedLevel >= value)
            return true;
        return false;
    }


}
[Serializable]
public class Stat
{
    public SkillObject skillType;
    public List<SkillAttributeObject> attributes = new List<SkillAttributeObject>();
    public Stat(SkillObject obj = null, List<SkillAttributeObject> att = null)
    {
        skillType = obj;
        attributes = att;
    }

}
[Serializable]
public class ReorderableStat : ReorderableList<Stat> { }
public class ReorderableList<T> : SimpleReorderableList
{
    public List<T> List;
}

[Serializable]
public class SimpleReorderableList { }
