﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Modifier", menuName = "Exp System/New Skill Modifier")]
public class SkillModifierObject : ScriptableObject, IModifiers {

    public int value;
    public void AddValue(ref int baseValue)
    {
        baseValue += value;
    }
}
