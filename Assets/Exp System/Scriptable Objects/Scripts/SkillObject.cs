﻿using System;
using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "Unamed Skill", menuName = "Exp System/New Skill")]
public class SkillObject : ScriptableObject
{
    public GameObject levelUpVfx;
}
