﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SkillController : MonoBehaviour
{
    public SkillReference skillReference;
    public SkillEvent _onLevelUp;
    public SkillEventWint _onXpGained;
    private int l_event, x_event;
    public List<Skill> GetSkills
    {
        get { return skillReference.Skills; }
    }

    private void OnEnable()
    {
        skillReference.SetUpSkills(this, OnLevelUp, OnXpGained);
        l_event = _onLevelUp.GetPersistentEventCount();
        x_event = _onXpGained.GetPersistentEventCount();
    }
    private void OnDisable()
    {
        skillReference.RemoveSkills();
    }
    public void AddSkillXp(SkillObject skill, int value)
    {
        skillReference.GetSkill(skill).AddSkillXP(value);
    }
    public bool HasRequiredLevel(SkillObject skill, int value)
    {
        if (skillReference.GetSkill(skill).ModifiedLevel >= value)
            return true;
        return false;
    }
    public void OnLevelUp(Skill skill)
    {
        if (skill.SkillType.levelUpVfx)
            Instantiate(skill.SkillType.levelUpVfx, transform.position + new Vector3(0, 2, 0), transform.rotation, transform);
        if (l_event > 0)
            _onLevelUp.Invoke(skill);
    }
    public void OnXpGained(Skill skill, int amount)
    {
        if (x_event > 0)
            _onXpGained.Invoke(skill, amount);
    }
}
public delegate void ModifiedEvent();
[Serializable]
public class SkillEvent : UnityEvent<Skill>
{

}
[Serializable]
public class SkillEventWint : UnityEvent<Skill, int>
{

}
[Serializable]
public class ModifiableInt
{
    int baseValue;
    public int BaseValue { get { return baseValue; } set { baseValue = value; UpdateModifiedValue(); } }
    public int ModifiedValue { get; private set; }
    public event ModifiedEvent ValueModified;
    public ModifiableInt(ModifiedEvent method = null)
    {
        ModifiedValue = baseValue;
        if (method != null)
            ValueModified += method;
    }

    public List<IModifiers> modifiers = new List<IModifiers>();
    public void RegisterModEvent(ModifiedEvent method)
    {
        ValueModified += method;
    }
    public void UnregisterModEvent(ModifiedEvent method)
    {
        ValueModified -= method;
    }
    public void UpdateModifiedValue()
    {
        var valueToAdd = 0;
        for (int i = 0; i < modifiers.Count; i++)
        {
            modifiers[i].AddValue(ref valueToAdd);
        }
        ModifiedValue = baseValue + valueToAdd;
        if (ValueModified != null)
        {
            ValueModified.Invoke();
        }
    }

    public void AddModifier(IModifiers modifier)
    {
        modifiers.Add(modifier);
        UpdateModifiedValue();
    }
    public void RemoveModifier(IModifiers modifier)
    {
        modifiers.Remove(modifier);
        UpdateModifiedValue();
    }
}