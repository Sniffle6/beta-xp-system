﻿
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class SkillReference : PropertyAttribute
{

    [Tooltip("Should the skill data be stored on a scriptable object or on the skill contorller? If the GameObject using this component is being created at runtime with multiple copies, this is recommended to be off.")]
    public bool storeDataLocally;
    public SkillListObject skillObjects;
    public SkillAlgorithmObject levelUpAlgorithm;
    public ReorderableStat Stats;
    private List<Skill> localSkills = new List<Skill>();

    public List<Skill> Skills
    {
        get { if (storeDataLocally) return localSkills; else return skillObjects.skills; }
    }
    public void SetUpSkills(SkillController controller, Action<Skill> levelUp, Action<Skill, int> xpGained)
    {
        Debug.Log("Setting up skills " + storeDataLocally);
        if (!storeDataLocally)
            skillObjects.SetUpSkills(controller, levelUp, xpGained);
        else
            for (int i = 0; i < Stats.List.Count; i++)
                localSkills.Add(new Skill(controller, Stats.List[i].skillType, Stats.List[i].attributes, 1, levelUpAlgorithm, levelUp, xpGained));
    }
    public ReorderableStat GetStats()
    {
        if (storeDataLocally)
            return Stats;
        else
            if (skillObjects)
            return skillObjects._stat;
        return null;
    }
    public void RemoveSkills()
    {
        if (!storeDataLocally)
            skillObjects.RemoveSkills();
    }
    public Skill GetSkill(SkillObject skill)
    {
        if (!storeDataLocally)
            return skillObjects.GetSkill(skill);
        else
            return localSkills[Array.IndexOf(Stats.List.ToArray(), skill)];
    }
    public Skill GetSkill(string skill)
    {
        if (!storeDataLocally)
            return skillObjects.GetSkill(skill);
        else
        {
            for (int i = 0; i < localSkills.Count; i++)
            {
                if (localSkills[i].SkillType.name == skill)
                    return localSkills[i];
            }
        }
            return localSkills[Array.IndexOf(Stats.List.ToArray(), skill)];
    }
    //public int GetSkillAttributeValue(SkillObject skill, SkillAttributeObject attribute)
    //{
    //   // var att = Array.IndexOf(GetSkill(skill).Attributes, attribute);
    //   // foreach (var att in GetSkill(skill).Attributes)
    //   // {
    //   //     if(att == attribute)
    //   //         return att.GetValue()
    //   // }
    //   //// skill.GetAttribute(attribute).GetValue(GetSkill(skill).ModifiedLevel, ref referenceValue);
    //    return 0;
    //}
    //public int GetSkillAttributeValue(SkillObject skill, int attribute)
    //{
    //    int output = 0;
    //    if (skill.attributes.Length > attribute)
    //        skill.attributes[attribute].GetValue(GetSkill(skill).ModifiedLevel, ref output);
    //    return output;
    //}
    public void AddModifier()
    {

    }
}
