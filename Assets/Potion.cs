﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Potion : Equipt
{
    protected bool active;
    public SkillModifier modifier;
    SkillModifier _modifier = new SkillModifier();
    public override void Action()
    {
        if (active)
        {
            active = false;
            character.skillReference.GetSkill(skill).Level.RemoveModifier(_modifier);
            StopCoroutine(OnUpdate());
        }
        else
        {
            _modifier.value = modifier.value;
            active = true;
            character.skillReference.GetSkill(skill).Level.AddModifier(_modifier);
            StartCoroutine(OnUpdate());
        }
    }
    IEnumerator OnUpdate()
    {
        while (active)
        {
            if (_modifier.value > 0)
            {
                character.skillReference.GetSkill(skill).Level.UpdateModifiedValue();
                _modifier.value -= 1;
                Debug.Log(_modifier.value);
            }
            else
            {
                active = false;
                character.skillReference.GetSkill(skill).Level.RemoveModifier(_modifier);
            }
            yield return new WaitForSeconds(1);
        }
    }
}
