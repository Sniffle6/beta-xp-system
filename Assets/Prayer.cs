﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Prayer : Equipt
{
    protected bool active;
    public SkillModifier modifier;
    public Image image;
    public override void Action()
    {
        if (active)
        {
            active = false;
            image.enabled = false;
            character.skillReference.GetSkill(skill).Level.RemoveModifier(modifier);
        }
        else
        {
            active = true;
            image.enabled = true;
            character.skillReference.GetSkill(skill).Level.AddModifier(modifier);
        }
    }

}
