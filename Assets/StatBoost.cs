﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class StatBoost : MonoBehaviour {

    Image image;

    private void Start()
    {
       image = GetComponent<Image>();
    }
    public void Action()
    {
        if (image.isActiveAndEnabled)
            image.enabled = false;
        else
            image.enabled = true;
    }
}
